use std::{cmp, convert::TryFrom, env};
use xcb::{xproto, Connection};

const ROOT_WINDOW_EVENT_MASK: xcb::EventMask =
    xcb::EVENT_MASK_SUBSTRUCTURE_REDIRECT | xcb::EVENT_MASK_SUBSTRUCTURE_NOTIFY;

fn main() {
    env::set_var("RUST_LOG", "info");
    pretty_env_logger::init();

    let (conn, screen) = Connection::connect(None) //
        .expect("failed to establish XCB connection");

    let root_window = conn
        .get_setup()
        .roots()
        .nth(screen as usize)
        .expect("cannot find the screen data")
        .root();

    xcb::change_window_attributes(
        &conn,
        root_window,
        &[(xcb::CW_EVENT_MASK, ROOT_WINDOW_EVENT_MASK)],
    )
    .request_check()
    .expect("cannot change the root event mask");

    while let Some(event) = conn.wait_for_event() {
        match event.response_type() {
            xcb::MAP_REQUEST => {
                handle_map_request(&conn, unsafe { xcb::cast_event(&event) });
            }
            xcb::CONFIGURE_REQUEST => {
                handle_configure_request(&conn, unsafe { xcb::cast_event(&event) });
            }
            xcb::MAP_NOTIFY => {
                handle_map_notify(&conn, unsafe { xcb::cast_event(&event) });
            }
            xcb::CONFIGURE_NOTIFY => {
                handle_configure_notify(&conn, unsafe { xcb::cast_event(&event) });
            }
            t => log::info!("unhandled event (response_type={})", t),
        }
    }
}

fn handle_map_request(conn: &Connection, event: &xproto::MapRequestEvent) {
    log::info!("Got an event: MAP_REQUEST (window={})", event.window());

    xcb::map_window(&conn, event.window())
        .request_check()
        .expect("cannot map a window");
}

fn handle_configure_request(conn: &Connection, event: &xproto::ConfigureRequestEvent) {
    log::info!("Got an event: CONFIGURE_REQUEST");
    log::info!("-> window = {}", event.window());
    log::info!("   x = {}", event.x());
    log::info!("   y = {}", event.y());
    log::info!("   width = {}", event.width());
    log::info!("   height = {}", event.height());
    log::info!("   border_width = {}", event.border_width());
    log::info!("   sibling = {}", event.sibling());
    log::info!("   stack_mode = {}", event.sibling());

    let mut values: Vec<(u16, u32)> = vec![];

    if event.value_mask() & xcb::CONFIG_WINDOW_X as u16 != 0 {
        values.push((
            xcb::CONFIG_WINDOW_X as u16,
            u32::try_from(cmp::max(0, event.x())).unwrap(),
        ));
    }

    if event.value_mask() & xcb::CONFIG_WINDOW_Y as u16 != 0 {
        values.push((
            xcb::CONFIG_WINDOW_Y as u16,
            u32::try_from(cmp::max(0, event.y())).unwrap(),
        ));
    }

    if event.value_mask() & xcb::CONFIG_WINDOW_WIDTH as u16 != 0 {
        values.push((xcb::CONFIG_WINDOW_WIDTH as u16, event.width().into()));
    }

    if event.value_mask() & xcb::CONFIG_WINDOW_HEIGHT as u16 != 0 {
        values.push((xcb::CONFIG_WINDOW_HEIGHT as u16, event.height().into()));
    }

    if event.value_mask() & xcb::CONFIG_WINDOW_BORDER_WIDTH as u16 != 0 {
        values.push((
            xcb::CONFIG_WINDOW_BORDER_WIDTH as u16,
            event.border_width().into(),
        ));
    }

    if event.value_mask() & xcb::CONFIG_WINDOW_SIBLING as u16 != 0 {
        values.push((xcb::CONFIG_WINDOW_SIBLING as u16, event.sibling()));
    }

    if event.value_mask() & xcb::CONFIG_WINDOW_STACK_MODE as u16 != 0 {
        values.push((
            xcb::CONFIG_WINDOW_STACK_MODE as u16,
            event.stack_mode().into(),
        ));
    }

    xcb::configure_window(&conn, event.window(), &values[..]);
    conn.flush();
}

fn handle_map_notify(_conn: &Connection, event: &xproto::MapNotifyEvent) {
    log::info!("Got an event: MAP_NOTIFY (window={})", event.window());
}

fn handle_configure_notify(_conn: &Connection, event: &xproto::ConfigureNotifyEvent) {
    log::info!("Got an event: CONFIGURE_NOTIFY");
    log::info!("-> window = {}", event.window());
}
